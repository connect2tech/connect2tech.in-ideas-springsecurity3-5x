package in.connect2tech.springsecurity.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

	@GetMapping("/showMyLoginPage")
	public String showMyLoginPage() {
		
		return "plain-login";
		
	}
	
	@GetMapping("/showMyLoginPage3")
	public String showMyLoginPage3() {
		
		return "plain-login3";
		
	}
	
	/* Role bases access */

	// add request mapping for /leaders

	@GetMapping("/leaders")
	public String showLeaders() {

		return "leaders";
	}

	// add request mapping for /systems

	@GetMapping("/systems")
	public String showSystems() {

		return "systems";
	}

	@GetMapping("/access-denied")
	public String accessDenied() {

		return "access-denied";
	}
}
